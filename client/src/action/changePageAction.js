import { MAIN_PAGE, CARD_SWIPE, CARD_FLY, CONTROL_PANEL } from './staticVariable';

export function changePageAction(page){    
    switch (page){
        case CARD_SWIPE:
            return{
                type:CARD_SWIPE
            }
        case CARD_FLY:        
            return{
                type:CARD_FLY
            }
        case CONTROL_PANEL:
            return{
                type:CONTROL_PANEL
            }
        default:
            return {
                type:MAIN_PAGE
            }
    }

}