import { LOGIN_HOMEPAGE, LOGIN_HOMEPAGE_FAIL,LOGIN_HOMEPAGE_ADMIN, LOGIN_HOMEPAGE_USER } from "./staticVariable";
import { checkLogin } from './../model/crudModel';
export function checkLoginAction(user){
    
    return dispatch=>{   
            
        checkLogin(user,(response)=>{  
            
            if(response.success){   
                
                dispatch(loginSuccess(response));
            }else{
                dispatch(loginNotSuccess());
            }
        });
        
    }
}

function loginSuccess(response){
    if(response.permission =="admin"){
        return {
            type:LOGIN_HOMEPAGE_ADMIN,
            user:response.user,
            userList:response.userList    
        }
    }else{
        return {
            type: LOGIN_HOMEPAGE_USER,
            user:response.user,
            userList:response.userList 
        }
    }
    
}
function loginNotSuccess(){   
    return {
        type: LOGIN_HOMEPAGE_FAIL   
    }
    
}

