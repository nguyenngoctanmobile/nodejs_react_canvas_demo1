
import React from "react";
import "./style.css";
import inputManager from "./../../logicjs/inputManager";
import Card from './../../logicjs/Models/Card';
import clientSocket from "socket.io-client";
const GameState ={
    StartScreen:0,
    Playing:1,
    GameOver:2
}
class CardFlyPage extends React.Component{    
    constructor(props){
        super(props)
        const width = 800;
        const height  = 600;
        const ratio = window.devicePixelRatio || 1;
        
        this.state ={         
            input:new inputManager(),
            screen:{
            width:width,
            height:height,
            ratio:ratio,
            },
            gameState:GameState.StartScreen,
            context:null
        }
        this.originalCard = [];
        this.newCard = [];
        this.startCreateNewCard=false; //check status when server send signal start create card to client 
        this.socket=null;
        this.enterFrame = 0; //that variable save the number of requrestAnimationFrame that to delete when finish game or stop game
        this.isRunningEnterframe = true; // status That use to check do or do not run update, because when delete requestAnimationFrame so update still run after        
        this.startAnimation = false;
    }
    componentDidMount(){
        this.socket = clientSocket("http://localhost:3000/fly");
       
        this.socket.on("createNewRectange",()=>{
            this.startCreateNewCard = true;  
                  
        })
        /*this.socket.on("disconnect",function(){           
            this.socket.disconnect();
        })*/
        this.socket.emit("newUser","andy",{user:"abc",pass:"124"});
       // this.state.input.bindKeys();
        const context = this.refs.canvas.getContext("2d");
        this.setState({context:context})       
       this.isRunningEnterframe= true;
       this.enterFrame = requestAnimationFrame(()=>{this.update()});       
        
    }
    componentWillUnmount(){        
        //this.state.input.unbindKeys();
        this.socket.disconnect();
        this.isRunningEnterframe = false;
        window.cancelAnimationFrame(this.enterFrame)    
        delete this.newCard;
        delete this.originalCard;
    }
    
    
    render(){       
        
        return(
            <div>
               <canvas ref="canvas" width={this.state.screen.width *this.state.screen.ratio} height = {this.state.screen.height *this.state.screen.ratio}/>
            </div>
        )
    }
    update(){        
       if(this.isRunningEnterframe){
        //const keys = this.state.input.pressedkeys;
        
        //Create Card
        if(this.startCreateNewCard ){  // have a signal but new card is still on the screen, must to run card out of screen and delete    
            if(!this.checkAllCardOnDie()){
                this.startAnimation = true;            
                for(let i =0;i<this.newCard.length;i++){
                    this.newCard[i].startAnimation();  //create the begining time to check card
                }            
            }
            this.startCreateNewCard = false;

        }
        if(this.startAnimation){         

            if(this.checkAllCardOnDie()){
                this.startAnimation =false;
                for(let i =0;i<this.newCard.length;i++){
                    this.newCard[i].resetCard()
                }
            }else{

                for(let i =0;i<this.newCard.length;i++){                    
                    if(!this.newCard[i].onDie){
                        this.newCard[i].update();
                    }
                }
            }
            
        }
        if(this.state.gameState === GameState.StartScreen){ 
            this.startGame();
        }
        if(this.state.gameState === GameState.Playing){ //rerender canvas when update
            
            this.clearBackGround();
            
            for(let j= 0;j<this.newCard.length;j++){               
                this.newCard[j].render(this.state);                            
            }             
            
            for(let i=0;i<this.originalCard.length;i++){
                this.originalCard[i].render(this.state);               
            }           
        }
        requestAnimationFrame(()=>{this.update()});
        }
    }
    
    startGame(){  
        this.initializeNewCard();
        this.initializeOrginalCard();
        this.setState({
            gameState:GameState.Playing
        })
    }
    initializeOrginalCard(){
         //create originalCard;
         let card;
         let pCardX = 110;
         let pCardY = 40;
         let imageBack = new Image();
        imageBack.src = "./assets/images/cardBack.png";
         for(let i = 0;i<3;i++) {        
             card = new Card({width:120,height:160,position:{x:pCardX,y:pCardY},onDie:false,speed:10,image:imageBack});
             pCardX+=(110 + 120);
             this.originalCard.push(card);
         }
         
    }
    initializeNewCard(){
        //create newCard;
        let card;
        let pCardX = 110;
        let pCardY = 40;
        let imageFront = new Image();
        imageFront.src = "./assets/images/cardFront.png";
        for(let i = 0;i<3;i++) {        
            card = new Card({width:120,height:160,position:{x:pCardX,y:pCardY},onDie:false,speed:10,image:imageFront});
            pCardX+=(110 + 120);
            this.newCard.push(card);
        }
    }  
    clearBackGround(){
        const context = this.state.context;
        context.save();
        context.scale(this.state.screen.ratio,this.state.screen.ratio);
        context.fillRect(0,0,this.state.screen.width,this.state.screen.height);
        context.globalAlpha=1;        
    }
    checkAllCardOnDie(){
        let countCardOnDie =0;
        for(let i = 0;i<this.newCard.length;i++){
            if(this.newCard[i].onDie) countCardOnDie++;
        }
        if(countCardOnDie === this.newCard.length){
            return true;
        }
        return false;
    }
    
}
export default CardFlyPage;
