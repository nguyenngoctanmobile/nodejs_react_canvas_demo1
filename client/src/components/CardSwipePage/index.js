
import React from "react";
import clientSocket from "socket.io-client";
import Card2 from './../../logicjs/Models/Card2';
const GameState ={
    StartScreen:0,
    Playing:1,
    GameOver:2
}
class CardSwipePage extends React.Component{
    constructor(props){
        super(props)
        this.socket =null;
        this.content = null;
        this.enterFrame = 0;
        this.isRunningEnterFrame = true;
        const width = 800;
        const height = 600;
        const ratio = window.devicePixelRatio || 1;
        this.state ={
            screen:{width:width,
            height:height,
            ratio:ratio           
            },
            context:null,
            gameState:GameState.StartScreen
        }
        this.card = null;
        this.startSwipeCard = false;
    }
    componentDidMount(){
        this.socket = clientSocket("http://localhost:3000/swipe");
        this.socket.on("createNewRectange",()=>{
            this.startSwipeCard= true;           
        })
        /*this.socket.on("disconnect",function(){           
            this.socket.disconnect();
        })*/
        this.socket.emit("newUser","andy",{user:"abc",pass:"124"});
       // this.state.input.bindKeys();
        const context = this.refs.canvas.getContext("2d");
        this.setState({context:context})       
       this.isRunningEnterframe= true;
       this.enterFrame = requestAnimationFrame(()=>{this.update()});
    }
    render(){
        return(
            <div>
               <canvas ref="canvas" width={this.state.screen.width *this.state.screen.ratio} height = {this.state.screen.height *this.state.screen.ratio}/>
            </div>
        )
    }
    componentWillUnmount(){
        
        this.isRunningEnterframe = false;
        this.socket.disconnect();
        window.cancelAnimationFrame(this.enterFrame)
        delete this.card;           
    }
    update(){
        if(this.isRunningEnterFrame){
            if(this.state.gameState === GameState.StartScreen){ 
                this.startGame();
            }
            if(this.state.gameState === GameState.Playing && this.card!=null && this.startSwipeCard){
                this.card.update();
                this.startSwipeCard=false;
            }
            if(this.state.gameState ===GameState.Playing && this.card!=null){
                this.clearBackGround();
                this.card.render(this.state);
                requestAnimationFrame(()=>{this.update()});
            }
        }
    }
    startGame(){
        this.initializeCard();
        this.setState({
            gameState:GameState.Playing
        })
    }
    initializeCard(){
        let imageFront = new Image();
        imageFront.src = "./assets/images/cardFront.png";
        let imageBack = new Image();
        imageBack.src = "./assets/images/cardBack.png";
        this.card = new Card2({width:120,height:160,position:{x:340,y:220},color:"#ffffff",imageFront:imageFront,imageBack:imageBack})
    }
    clearBackGround(){
        const context = this.state.context;
        context.save();
        context.scale(this.state.screen.ratio,this.state.screen.ratio);
        context.fillRect(0,0,this.state.screen.width,this.state.screen.height);
        context.globalAlpha=1;        
    }
}
export default CardSwipePage;