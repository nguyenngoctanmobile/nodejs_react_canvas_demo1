import React from "react"
import clientSocket from "socket.io-client";

class ControlPanelPage extends React.Component{
    constructor(props){
        super(props)
        this.socket=null;
        this.startAction =this.startAction.bind(this)
    }
    componentDidMount(){
        this.socket = clientSocket("http://localhost:3000/admin");        
    }
    componentWillUnmount(){
        this.socket.disconnect();
    }
    render(){
        return(
            <div>
               <h1>CONTROL PANEL</h1>
               <ul>
                   <li><button name = "startFly" onClick = {this.startAction}>Start Fly</button></li>
                   <li><button name = "startSwipe" onClick = {this.startAction}>Start Swipe</button></li>
               </ul>
            </div>
        )
    }
    startAction(e){
        if(e.target.name =="startFly"){
            this.socket.emit("startFly");
        }else{
            this.socket.emit("startSwipe");
        }
    }
}
export default ControlPanelPage;
