
import React from "react";


export default class LoginForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            username:"user",
            password:"user",
            isError:false,
            isSubmit:false
        }
        this.onSubmitData = this.onSubmitData.bind(this);
        this.onInputChangeData = this.onInputChangeData.bind(this);      
    }
    componentDidMount(){        
        this.usernameinput.focus();        
    }
    static getDerivedStateFromProps(nextProps, prevState) {
       
        if(nextProps.isError!=prevState.isError){            
            
            prevState.isError = nextProps.isError;
            prevState.username = "";
            prevState.password = "";
            return prevState;        
        }   
      
    }    
    
    render(){        
        let errorTag;
        if(this.state.isError){                      
            errorTag=<p>Username or Password doesn't correct, please try again</p>  
            
        }
            
        return(            
            <form onSubmit =  {this.onSubmitData}>
                <label>User name:<input type="text" name = "username"  onChange = {this.onInputChangeData} value={this.state.username} ref={(input)=>{this.usernameinput=input}} /></label>
                <label>Password: <input type="password" name="password" onChange = {this.onInputChangeData} value = {this.state.password}/></label>
                <input type="submit" value="Submit"/> 
                {errorTag}   
            </form>            
            
        )
    }
    componentDidUpdate(){        
        if(this.state.isError && this.state.isSubmit){            
            this.usernameinput.focus();
            this.setState({
                isSubmit:false,
            });
        }
    }
    
    onInputChangeData(e){
        this.setState({
            [e.target.name]:e.target.value
        })        
    }
    onSubmitData(e){       
        e.preventDefault();
        let user = {
            username:this.state.username,
            password:this.state.password
        }
        this.setState({
            isSubmit:true,
        });
        this.props.checkLoginAction(user);          
        

    }
}



