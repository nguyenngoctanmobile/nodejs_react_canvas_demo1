import React from "react"
import NavigationPage from "../NavigationPage";
class HeaderPage extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div>
                <div>
                    <h1>
                        DEMO SOCKET.IO
                    </h1>
                </div>
                <NavigationPage/>
            </div>
        )
    }
}
export default HeaderPage;
