import React from "react";

import { connect } from 'react-redux';
import LoginForm from "../Forms/LoginForm";
import { checkLoginAction } from './../../action/crudAction';

class HomePage extends React.Component{
    constructor(props){
        super(props);       
        
    }    
    
    render(){      
        let renderContent;       
        if(this.props.userReducer.isLogin){
            if(this.props.userReducer.permission == "admin"){
                renderContent = <div>
                    <p>Hello {this.props.userReducer.user.username}</p>
                    <p> Welcome to Demo Socket.IO</p>
                </div>
            }else{
                renderContent = <div>
                    <p>Hello {this.props.userReducer.user.username}</p>
                    <p> Welcome to Demo Socket.IO</p>
                </div>
            }
        }else{                
            renderContent = <LoginForm checkLoginAction={this.props.checkLoginAction} isError={this.props.userReducer.isError}/>          
           
        }
        return(
            <div>
                {renderContent}               
            </div>
        )
    }
    
}
function mapStateToProp(state){
    return{
        userReducer:state.userReducer
    }
  }
export default connect(mapStateToProp,{checkLoginAction})(HomePage)
