import React from "react";
import { CARD_SWIPE, CARD_FLY, CONTROL_PANEL } from '../../action/staticVariable';
import CardSwipePage from './../CardSwipePage';
import CardFlyPage from './../CardFlyPage';
import HomePage from './../HomePage';
import ControlPanelPage from "../ControlPanelPage";
export default class MainPage extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        let currentPage;               
    switch(this.props.currentPage){
        case CARD_SWIPE:
          currentPage = <CardSwipePage/>;
          break;
        case CARD_FLY:
          currentPage = <CardFlyPage/>;
          break;
        case CONTROL_PANEL:
          currentPage = <ControlPanelPage/>
          break;
        default:
          currentPage = <HomePage/>
          break;
    } 
        return(
            <div>
              {currentPage}
            </div>
        )
    }
}


