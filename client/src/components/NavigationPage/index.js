
import React from "react";
import {changePageAction} from "./../../action/changePageAction";
import {connect} from "react-redux";
import { MAIN_PAGE, CARD_SWIPE, CARD_FLY, CONTROL_PANEL } from '../../action/staticVariable';
import './style.css';
class NavigationPage extends React.Component{
    constructor(props){
        super(props)
        this.buttonClickChangePage = this.buttonClickChangePage.bind(this);

    }
    render(){
        let navTag;
        if(this.props.userReducer.isLogin){
            if(this.props.userReducer.permission=="user"){
                navTag = 
                            <ul>
                                <li><button name ={MAIN_PAGE} value = "MainPage" onClick = {this.buttonClickChangePage}>Home</button></li>
                                <li><button name ={CARD_SWIPE} value = "CardSwipePage" onClick = {this.buttonClickChangePage} >CardSwipe</button></li>
                                <li><button name ={CARD_FLY} value = "CardFlyPage" onClick = {this.buttonClickChangePage}> CardFly</button></li>
                            </ul>
            }else{
                navTag = 
                            <ul>
                                <li><button name ={MAIN_PAGE} value = "MainPage" onClick = {this.buttonClickChangePage}>Home</button></li>
                                <li><button name ={CONTROL_PANEL} value = "ControlPanel" onClick = {this.buttonClickChangePage} >Control Panel</button></li>  
                            </ul>
            }
                          
        }else{
            navTag = 
                        <ul>
                            <li><button name ={MAIN_PAGE} value = "MainPage" onClick = {this.buttonClickChangePage}>Login</button></li>                
                        </ul>
                       
        }
        return(
            <nav>
                {navTag}
            </nav>
        )
    }
    buttonClickChangePage(e){
       
       switch(e.target.name){
           case CARD_SWIPE:                
                this.props.changePageAction(CARD_SWIPE);
                break;
            case CARD_FLY:
                this.props.changePageAction(CARD_FLY);
                break;
            case CONTROL_PANEL:
                this.props.changePageAction(CONTROL_PANEL);
                break;
            default:
                this.props.changePageAction(MAIN_PAGE);
                break;
       }
    }
}
function mapStateToProp(state){
    return{
       userReducer:state.userReducer
    }
}
let  NavigationPageContainter = connect(mapStateToProp,{changePageAction})(NavigationPage)
export default NavigationPageContainter;

