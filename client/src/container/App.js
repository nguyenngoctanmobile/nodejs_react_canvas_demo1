import React, { Component } from 'react';
import './App.css';
import HeaderPage from '../components/HeaderPage';
import MainPage from './../components/MainPage';
import FooterPage from './../components/FooterPage';
import {connect} from "react-redux";

class App extends Component {

  constructor(props){
    super(props);
  }
  render() {    
    return (
      <div className="App">    
        <header className="App-header">
               <HeaderPage/>  
        </header>                
        <main  className="App-main">
            <MainPage currentPage = {this.props.routerPageReducer.currentPage}/>
        </main>     
        <footer className="App-footer">
              <FooterPage/>
        </footer>
      </div>
    );
  }
}
function mapStateToProp(state){
  return{
    routerPageReducer:state.routerPageReducer,
    userReducer:state.userReducer
  }
}

export default connect(mapStateToProp)(App);
