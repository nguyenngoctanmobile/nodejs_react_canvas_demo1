export default class Card{
    constructor(args){        
        this.width = args.width;
        this.height = args.height;
        this.position = Object.assign({},args.position);
        this.currrentPosition = Object.assign({},args.position);     
        this.onDie = args.onDie;
        this.speed = args.speed;
        this.image = args.image;
        this.startTime =0;
         
        
         
    }
    startAnimation(){
        this.startTime = new Date().getTime();
    }
    resetCard(){
        this.currrentPosition.y = this.position.y;
        this.onDie = false;
        this.startTime = 0;
    }
    update(){
        if(!this.onDie){
           
            let currentTime = new Date().getTime();
            let minusTime = currentTime -this.startTime;
            if((minusTime > 500 && minusTime<800) ||(minusTime>2000 && minusTime<3000)){      
                    
                this.currrentPosition.y +=this.speed;
            }
            if(this.currrentPosition.y>(600+this.height) || minusTime>3000){
                this.onDie = true;                
                this.currrentPosition.y = this.position.y;
                this.startTime = 0;
            }
        }       
    }    

   
    render(state){
        if(!this.onDie){
            const context = state.context;        
            context.save();       
            context.translate(0,0);
            
            context.drawImage(this.image, this.currrentPosition.x, this.currrentPosition.y, this.width, this.height);
            
            context.restore();
        }
    }

}