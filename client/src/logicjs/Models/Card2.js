
export default class Card2{
    
    constructor(args){
        this.width = args.width;
        this.height = args.height;
        this.position=args.position;
        this.color = args.color;
        this.imageFront = args.imageFront;
        this.imageBack = args.imageBack;       
        this.isStartSwipe = false;
        this.flipStep = 10;
        this.isWhite = true;
        this.getBeginTimeToSwipe=0;
        this.currentImage=this.imageBack;
        this.newDataSwipe ={
            newWidth:args.width,
            newPositionX:0
        } 
    }
    update(){
        this.isStartSwipe = true;        
        this.getBeginTimeToSwipe = new Date().getTime();
        
    }
    render(state){
                     
        const context = state.context;        
        context.save();       
        context.translate(0,0);              
        if(this.isStartSwipe){           
            let newTime = new Date().getTime();

            if(newTime - this.getBeginTimeToSwipe >=80){         
                this.flipStep--;                
                if(this.flipStep>5){
                    this.newDataSwipe.newWidth = this.width * (0.2*(this.flipStep - 6));  
                }else{
                    if(this.flipStep == 5){                        
                        if(this.currentImage === this.imageFront){
                            this.currentImage = this.imageBack;
                        }else{
                            this.currentImage = this.imageFront;
                        }
                    }
                    this.newDataSwipe.newWidth = this.width * (0.2*(5 -this.flipStep));
                }            
                
                this.getBeginTimeToSwipe = newTime;    
           
                if(this.flipStep == 0){
                    this.isStartSwipe=false;
                    this.flipStep = 10;                   
                }
               
            }
            this.newDataSwipe.newPositionX = this.position.x+((this.width - this.newDataSwipe.newWidth)/2);           
            context.drawImage(this.currentImage, this.newDataSwipe.newPositionX, this.position.y, this.newDataSwipe.newWidth, this.height);                
            
        }else{
            context.drawImage(this.currentImage, this.position.x, this.position.y, this.width, this.height);            
             
        }
        
        context.restore();

        
    }
}