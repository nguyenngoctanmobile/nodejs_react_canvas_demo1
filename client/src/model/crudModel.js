export const  checkLogin=(user,callback)=>{
    return fetch("/api/login",{
        method:'post',
        body:JSON.stringify(user),
        headers:{
            "Accepts":"application/json",
            "Content-type":"application/json"
        }
    }).then(checkStatus).then(parseJSON).then(callback);
}

function checkStatus(response){
    /*if(response.status>=200 && response.status< 300){
       
        return response;
    }else{
        const err = new Error(`HTTP Error ${response.statusText}`);
        err.status = response.status;
        err.response = response;
        throw err;
         
    }*/
    return response;
}

function parseJSON(response){    
    return response.json();
}