import {combineReducers} from "redux";
import routerPageReducer from "./routePageReducer";
import socketReducer from "./socketReducer";
import userReducer from "./userReducer";
export default combineReducers({
    routerPageReducer:routerPageReducer,
    socketReducer:socketReducer,
    userReducer:userReducer
})