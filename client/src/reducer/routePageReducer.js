
import { CARD_SWIPE, CARD_FLY, MAIN_PAGE, CONTROL_PANEL } from '../action/staticVariable';
const initialPage ={
    currentPage:MAIN_PAGE
}
export default (state=initialPage,action={})=>{    
    switch(action.type){
        case CARD_SWIPE:            
            return{
                currentPage:CARD_SWIPE
            }
        case CARD_FLY:           
            return {
                currentPage:CARD_FLY
            }
        case CONTROL_PANEL:
            return{
                currentPage:CONTROL_PANEL
            }
        case MAIN_PAGE:           
            return {
                currentPage:MAIN_PAGE
            }
        default:              
            return state;
    }
}