
import { SOCKET_RECEIVE, SOCKET_SEND, LOGIN_HOMEPAGE } from './../action/staticVariable';
const initialPage = {
    data:{}
   
}

export default (state=initialPage,action={})=>{
    switch(action.type){
        case LOGIN_HOMEPAGE:
            return {
                data:action.data               
            }
      
        default:
            return state;
            

    }
}
