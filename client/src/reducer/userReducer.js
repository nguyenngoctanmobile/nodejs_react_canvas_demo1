import { LOGIN_HOMEPAGE, LOGIN_HOMEPAGE_USER } from "../action/staticVariable";
import { LOGIN_HOMEPAGE_FAIL, LOGIN_HOMEPAGE_ADMIN } from './../action/staticVariable';

const  initializeUser = {
    user:{},
    isLogin:false,
    userList:{},
    isError:false
    
}

export default (state=initializeUser,action={})=>{
    switch(action.type){
        case LOGIN_HOMEPAGE:   
            return Object.assign({},state,{
                user : action.user,
                userList : action.userList,
                isLogin : true,
                isError:false
            });
        case LOGIN_HOMEPAGE_ADMIN:
            return Object.assign({},state,{
                user : action.user,
                userList : action.userList,
                isLogin : true,
                isError:false,
                permission:"admin"
            });
        case LOGIN_HOMEPAGE_USER:
            return Object.assign({},state,{
                user : action.user,
                userList : action.userList,
                isLogin : true,
                isError:false,
                permission:"user"
            });
        case LOGIN_HOMEPAGE_FAIL:           
            return Object.assign({},state,{
                user : {},
                userList : {},
                isLogin : false,
                isError:true
            });
        default:            
            return state;
        
    }
}

