var express = require("express");
var app  = express();
var path  =require("path");
var bodyParser = require("body-parser");
var socketRouter = require("./routes/socketRouter");
var userAPI = require("./routes/userAPI");
var port = process.env.port || 3000;


app.use(express.static(path.resolve(__dirname,"./../client/build/")));

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get("/",function(req,res){
    res.sendFile(path.resolve(__dirname,"build/index.html"));
})

app.use("/socket",socketRouter);  //Route for socket
app.use("/api",userAPI);
/*app.listen(port,function(err){
    if(err){
        console.log("======Error Server Can not Start=======")
    }else {
        console.log("=======Server is Already=========")
    }
})*/


//socket.io inititalize
var socket = require("socket.io");
var serverSocket = require("http").createServer(app).listen(port,function(){
   // console.log("Express server listnening on port:"+ app.get('port'));
});

var io = socket.listen(serverSocket);

//var io = require("socket.io")()
//

let clientStartFlyList = {};
let clientStartSwipeList = {};
let clientAdminList = {};
let sendDataInterval;
let userIdFlyCount = 0;
let userIdSwipeCount = 0;
let userIdAdminCount = 0;

//Socket.io Start
var ioFly = io.of("/fly");
ioFly.on('connection',(client)=>{

    client.id = "userFly_"+userIdFlyCount;        
    clientStartFlyList[client.id] = client;
    userIdFlyCount++;
    client.on('disconnect',()=>{
        delete clientStartFlyList[client.id];               
    });
   
    
});
var ioSwipe = io.of("/swipe");
ioSwipe.on('connection',(client)=>{
    client.id = "userSwipe_"+userIdSwipeCount;
    clientStartSwipeList[client.id]=client;
    userIdSwipeCount++;
    client.on('disconnect',()=>{
        delete clientStartSwipeList[client.id]        
    })
  
});
var ioAdmin = io.of("/admin");
ioAdmin.on('connection',(client)=>{
    client.id = "userAmdin_"+userIdAdminCount;
    clientAdminList[client.id] = client;
    userIdAdminCount++;
    client.on('disconnect',()=>{
        delete clientAdminList[client.id]        
    })
    
    client.on("startFly",()=>{       
        let clientArr = Object.keys(clientStartFlyList); 
        for(let i=0;i<clientArr.length;i++){
            clientStartFlyList[clientArr[i]].emit("createNewRectange");            
        }
    })
    client.on("startSwipe",()=>{      
        let clientArr = Object.keys(clientStartSwipeList); 
        for(let i=0;i<clientArr.length;i++){
            clientStartSwipeList[clientArr[i]].emit("createNewRectange");            
        }
    })
});
/*
    io.on('connection',(client)=>{
        
       
        client.id = "user_"+userIdCount;
        
        clientList[client.id]=client;
       
        userIdCount ++;

        client.on('disconnect',()=>{
            console.log("client "+client.id+" is removed");  
            delete clientList[client.id];
            let clientListArr = Object.keys(clientList);
            if(sendDataInterval && clientListArr.length==0){   
                 console.log("clear interval")
                clearInterval(sendDataInterval);
                sendDataInterval=false;
            }
        });
        client.on('newUser',(name,data)=>{  //client send signal 'new user' to server
            console.log("new user is created :"+name)
        })
        
        client.emit("FirstMessageToClient",{  //send signal to client
            msg:"Hello Client your id is :"+ client.id,
            id:client.id
        });        
       
         // broastcast dta
         if(!sendDataInterval){ 
            console.log("create interval")          
            sendDataInterval = setInterval(function(){
                let clientArr = Object.keys(clientList);    
                console.log("sent signal to client")           
                if(clientArr.length>0){
                    for(let i in clientArr){
                        clientList[clientArr[i]].emit("createNewRectange"); 
                                           
                    }
                }
            },4000) //>3000
        }
        //
        

    });
   
    
   */
    

    //io.emit("eventname",{}) //this is broastcast
//
