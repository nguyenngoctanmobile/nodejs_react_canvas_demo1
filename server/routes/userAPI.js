
let express =require('express');
let app  = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let fs =require("fs");
let path = require("path");
let Router = express.Router();

Router.post("/login",(req,res)=>{
    var user = req.body;
    
    if(user){       
        fs.readFile(path.join(__dirname,"./../data/userList.json"),(err,data)=>{
            if(!err){
                /*io.emit('loginsuccess',{
                    message:"welcome to socket.io",
                    yourid:"123454"
                });
                */
               var dataToReponse  = JSON.parse(data);
               var resultCheck = checkUser(user,dataToReponse);
                if( resultCheck ){
                    if(resultCheck == "admin" ){
                        res.json({"success":true,
                            "user":user,
                            "userList":dataToReponse,
                            "permission":"admin"
                        })
                    }else{
                        res.json({"success":true,
                            "user":user,
                            "userList":dataToReponse,
                            "permission":"user"
                        })
                    }
                }else{
                    res.status(500);
                    res.json({"success":false,"error":err});
                }
                
            }else{
                res.status(500);
                res.json({"success":false,"error":err});
            }
        });
        
    }else{
        res.status(500);
        res.json({"success":false,"error":""});
    }
      
});

function checkUser(user,userList){
    for(let i = 0;i<userList.length;i++){
        if(user.username == userList[i].username &&user.password == userList[i].password){
            if(user.username == "admin"){
                return "admin";
            }else{
                return "user";
            }
            
        }
    }
    return false;
}
module.exports =  Router;